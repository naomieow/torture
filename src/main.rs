use valence::{
    prelude::*, 
    DefaultPlugins,
    entity::{
        display::Scale, 
        text_display::{
            TextDisplayEntityBundle, 
            Background
        }
    }, 
    network::{
        async_trait, 
        BroadcastToLan
    },
    client::{
        interact_block::InteractBlockEvent, 
    },
    protocol::packet::sound::Sound,
    protocol::packet::sound::SoundCategory,
};

mod chat;

// Set spawn height at 64
const SPAWN_Y: i32 = 64;

struct TortureCallbacks;

#[async_trait]
impl NetworkCallbacks for TortureCallbacks {
    // Allow the server to appear on LAN menu instead of having to type `localhost`
    async fn broadcast_to_lan(&self, _shared: &SharedNetworkState) -> BroadcastToLan {
        BroadcastToLan::Enabled("Connect!".into())
    }
}

fn main() {
    let mut app = App::new();
    build_app(&mut app);
    app.run();
}

fn build_app(app: &mut App) {
    app.insert_resource(NetworkSettings {
        connection_mode: ConnectionMode::Offline, // Offline so I can connect through packet_inspector proxy - SET TO ONLINE FOR RELEASE
        callbacks: TortureCallbacks.into(), // Set callbacks for LAN broadcast
        ..Default::default()
    })
    .add_plugins(DefaultPlugins)
    .add_plugin(chat::ChatMod) // Add in chat handler
        .add_systems(Startup, setup)
        .add_systems(Update, (
            init_clients, // Initialisze clients when they join
            despawn_disconnected_clients, // Remove clients when they leave so they can join again
            break_cracked_deepslate_tiles, // Add tile break system
        ),
    )
    .run();
}

fn setup(
    mut commands: Commands,
    server: Res<Server>,
    mut dimensions: ResMut<DimensionTypeRegistry>,
    biomes: Res<BiomeRegistry>,
) {
    dimensions.insert(
        ident!("overworld"),
        DimensionType {
            ambient_light: 0.0,
            has_skylight: false,
            has_ceiling: false,
            natural: false,
            ..Default::default()
        },
    );

    let mut instance = Instance::new(ident!("overworld"), &dimensions, &biomes, &server); // Create world instance

    // Create instance chunks
    for z in -5..5 {
        for x in -5..5 {
            instance.insert_chunk([x, z], Chunk::default());
        }
    }

    // Create starting room
    // TODO: Either move to own method or figure out valence_anvil
    for z in -4..5 {
        for x in -4..5 {
            instance.set_block([x, SPAWN_Y, z], BlockState::DEEPSLATE_TILES);
            instance.set_block([x, SPAWN_Y+4, z], BlockState::DEEPSLATE_TILES);
        }
        for y in SPAWN_Y+1..=SPAWN_Y+3 {
            instance.set_block([-4, y, z], BlockState::DEEPSLATE_TILES);
            instance.set_block([4, y, z], BlockState::DEEPSLATE_TILES);
        }
    }

    for x in -4..5 {
        for y in SPAWN_Y+1..=SPAWN_Y+3 {
            instance.set_block([x, y, -4], BlockState::DEEPSLATE_TILES);
            instance.set_block([x, y, 4], BlockState::DEEPSLATE_TILES);
        }
    }
    instance.set_block([-4, SPAWN_Y+2, -2], BlockState::CRACKED_DEEPSLATE_TILES);
    instance.set_block([-4, SPAWN_Y+3, -2], BlockState::CRACKED_DEEPSLATE_TILES);

    let instance_id = commands.spawn(instance).id();

    // commands.spawn(BlockDisplayEntityBundle {
    //     location: Location(instance_id),
    //     position: Position(DVec3::new(-4.0, SPAWN_Y as f64 + 2.0, -2.0)),
    //     block_display_block_state: valence::entity::block_display::BlockState(BlockState::CRACKED_DEEPSLATE_TILES),
    //     display_scale: Scale(Vec3::from([1f32,1f32,1f32])),
    //     ..Default::default()
    // });

    // commands.spawn(BlockDisplayEntityBundle {
    //     location: Location(instance_id),
    //     position: Position(DVec3::new(-4.0, SPAWN_Y as f64 + 3.0, -2.0)),
    //     block_display_block_state: valence::entity::block_display::BlockState(BlockState::CRACKED_DEEPSLATE_TILES),
    //     display_scale: Scale(Vec3::from([1f32,1f32,1f32])),
    //     ..Default::default()
    // });

    // Create vaguely ominous message for players
    commands.spawn(TextDisplayEntityBundle {
        location: Location(instance_id),
        position: Position(DVec3::new(0.5, SPAWN_Y as f64 + 2.5, 4.0)),
        text_display_text: valence::entity::text_display::Text("Have fun!".into_text().bold().color(Color::DARK_RED)),
        look: Look::new(180.0, 0.0),
        text_display_background: Background(0),
        display_scale: Scale(Vec3::from([1.5f32, 1.5f32, 1.5f32])),
        ..Default::default()
    });

}

fn break_cracked_deepslate_tiles(
    mut events: EventReader<InteractBlockEvent>,
    mut instances: Query<&mut Instance>,
) {
    let mut instance = instances.single_mut();

    for event in &mut events {
        let pos = event.position; // Grab the position of the block being interacted with
        let dvec_pos = DVec3::new(pos.x as f64, pos.x as f64, pos.x as f64); // Cast pos into DVec3 because Instance::play_sound and Instance::play_particle require a DVec3 position
        let block = instance.block(pos).unwrap(); // Get block at pos
        if block.state() == BlockState::CRACKED_DEEPSLATE_TILES { // Check if block matches Cracked Deepslate Tiles
            instance.set_block(pos, BlockState::AIR); // Set block at position to air
            instance.play_sound(Sound::BlockDeepslateTilesBreak, SoundCategory::Block, dvec_pos, 100.0, 0.5); // Play breaking sound
            // TODO: Add particle effect, below doesn't work
            // instance.play_particle(&Particle::Smoke, false, dvec_pos, Vec3::new(1.0, 1.0, 1.0), 0.3, 50);
        }
    }
}

fn init_clients(
    mut clients: Query<
        (
            &mut Location,
            &mut Position,
            &mut HasRespawnScreen,
            &mut GameMode,
        ),
        Added<Client>,
    >,
    instances: Query<Entity, With<Instance>>,
) {
    // TODO: Make code better :3
    for (mut loc, mut pos, mut has_repsawn_screen, mut game_mode) in &mut clients {
        loc.0 = instances.iter().next().unwrap();
        pos.set([0.5, SPAWN_Y as f64 + 1.0, 0.5]);
        has_repsawn_screen.0 = true;
        *game_mode = GameMode::Adventure;
    }
}
