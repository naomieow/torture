use valence::{
    prelude::*, client::message::{ChatMessageEvent, SendMessage},
};

pub struct ChatMod;

impl Plugin for ChatMod {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, handle_chat_message);
        app.add_systems(Update, handle_join_message);
    }
}

// TODO: Make join message show for every player
fn handle_join_message(
    mut clients: Query<(&mut Client, &Username), Added<Client>>,
    // usernames: Query<&Username>,
) {
    for (mut client, username) in &mut clients {
        client.send_chat_message(username.0.clone().into_text().color(Color::YELLOW) + " joined the game".into_text().color(Color::YELLOW))
    }
}

fn handle_chat_message( // Allow players to send messages in chat
    mut clients: Query<&mut Client>,
    usernames: Query<&Username>,
    mut events: EventReader<ChatMessageEvent>,
) {
    for event in events.iter() {
        let username = usernames.get(event.client).map(|username| username.0.clone()).unwrap_or(String::from("?")); // Grab player username
        let msg = "<".into_text() + username + "> " + event.message.as_ref().to_owned();

        for mut client in &mut clients {
            client.send_chat_message(msg.clone());
        }
    }
}